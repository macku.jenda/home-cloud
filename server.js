/* Node modules */
const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const yargs = require('yargs');
// const request = require('request');

/* Setup server */
const app = express();
const argv = yargs
  .command('dev', 'Run server in developer mode', {
    port: {
      describe: 'Port where server will run. Default is 3001',
      demand: false,
      alias: 'p'
    }
  })
  .help()
  .argv;
const command = argv._[0];
let mode = '';

/* --------------------------------------------------- */

// Only for developing
if(command === 'dev') {
  mode = 'in developer mode';
  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('content-type', 'application/json');
    next();
  });
}

const port = argv.port ? process.env.PORT || argv.port : process.env.PORT || 3001;

const api = require('./back-end/routes/api');
const film = require('./back-end/routes/film');
const disk = require('./back-end/routes/disk');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(__dirname + '/front-end/dist'));
app.use('/api', api);
app.use('/api/film', film);
app.use('/api/disk', disk);

app.get('*', (req, res) => res.sendFile(path.join(__dirname+ '/front-end/dist/index.html')));

/* --------------------------------------------------- */
const server = http.createServer(app);

/**
 * Create server witch listen on port XY
 */
server.listen(port, () => {
  console.log(`Server running on 192.168.3.124:${port} ${mode}`);
});
