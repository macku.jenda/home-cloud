const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.send('Home Cloud API');
});

module.exports = router;