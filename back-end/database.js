const inputChecker = require('./security/input-checker');

/**
 * Method for returning Access object for database
 * @return {{user: string, database: string, password: string, port: number}}
 */
const getAccess = () => {
  return {
    user: 'pi',
    database: 'homecloud',
    password: 'Macku1Jan12',
    port: 5432
  };
};

/* ---------------------- Films --------------------- */

/**
 *
 * @param name
 * @param year
 * @return {string}
 */
let getFilteredListOfFilms = (name, year) => {
  let filmName = name ? `LIKE '${name}%'` : 'IS NOT NULL';
  let filmYear = inputChecker.isYear(year) ? `= '${year}-01-01 00:00:00.000000'` : 'IS NOT NULL';

  return 'SELECT film.id, film_name, film_note, film_year, file_type_shortcut, film_next_film_id, film_prev_film_id, string_agg(genre_name, \', \') AS genre_name, string_agg(film_tag_name, \', \') AS film_tag_name\n' +
         'FROM film\n' +
         '  LEFT JOIN film_cloud_file f2 ON film.id = f2.film_id\n' +
         '  LEFT JOIN cloud_file c2 ON f2.cloud_file_id = c2.id\n' +
         '  LEFT JOIN file_type f ON c2.file_type_id = f.id\n' +
         '  LEFT JOIN state s2 ON c2.state_id = s2.id\n' +
         '  LEFT JOIN film_genre g ON film.id = g.film_id\n' +
         '  LEFT JOIN genre g2 ON g.genre_id = g2.id\n' +
         '  LEFT JOIN film_tag_film ftf ON film.id = ftf.film_id\n' +
         '  LEFT JOIN film_tag t ON ftf.film_tag_id = t.id\n' +
         `WHERE state_name = \'active\' AND film_name ${filmName} AND film_year ${filmYear}\n` +
         'GROUP BY film.id, file_type_shortcut\n' +
         'ORDER BY film_name ASC;';
};

/**
 *
 * @return {string}
 */
const getFilmById = () => {
  return 'SELECT film_name, film_note, film_year, file_type_shortcut, film_next_film_id, film_prev_film_id\n' +
         'FROM film\n' +
         '  LEFT JOIN film_cloud_file f2 ON film.id = f2.film_id\n' +
         '  LEFT JOIN cloud_file c2 ON f2.cloud_file_id = c2.id\n' +
         '  LEFT JOIN file_type f ON c2.file_type_id = f.id\n' +
         '  LEFT JOIN state s2 ON c2.state_id = s2.id\n' +
         'WHERE state_name = \'active\' AND film.id = $1';
};

/**
 *
 * @return {string}
 */
const getFilmSource = () => {
  return 'SELECT film_name, cloud_file_path, file_type_shortcut\n' +
         'FROM film\n' +
         '  LEFT JOIN film_cloud_file f2 ON film.id = f2.film_id\n' +
         '  LEFT JOIN cloud_file c2 ON f2.cloud_file_id = c2.id\n' +
         '  LEFT JOIN file_type f ON c2.file_type_id = f.id\n' +
         '  LEFT JOIN state s2 ON c2.state_id = s2.id\n' +
         'WHERE state_name = \'active\' AND film.id = $1';
};

/**
 * SQL for post film and cloud file for new film insert
 * SQL Params:
 * $1 {string} - Film name
 * $2 {string} - Film note
 * $3 {number} - Film state
 * $4 {timestamp} - Film year
 * $5 {number} - Added by user
 * $6 {string} - File path
 * $7 {number} - File size
 * $8 {number} - File state
 * $9 {number} - File type
 * @return {string}
 */
const postNewFilm = () => {
  return 'WITH data (film, note, film_state, year, add_time, by_user, path, size, file_state, type) AS (\n' +
         '  VALUES ($1, $2, $3, $4, current_timestamp, $5, $6, $7, $8, $9)\n' +
         '), ins1 AS (\n' +
         '  INSERT INTO film (film_name, film_note, state_id, film_year, film_time_of_add, film_added_by_user_id)\n' +
         '    SELECT film, note, film_state, year, add_time, by_user FROM data\n' +
         '    ON CONFLICT DO NOTHING\n' +
         '  RETURNING id AS f_id\n' +
         '), ins2 AS (\n' +
         '  INSERT INTO cloud_file (cloud_file_path, cloud_file_size, state_id, file_type_id, cloud_file_time_of_add, "cloud_file_added_by user_id")\n' +
         '    SELECT path, size, file_state, type, add_time, by_user FROM data\n' +
         '  RETURNING id AS c_f_id\n' +
         ')\n' +
         'INSERT INTO film_cloud_file (film_id, cloud_file_id)\n' +
         '  VALUES ((SELECT f_id FROM ins1), (SELECT c_f_id FROM ins2));'
};

/**
 *
 */
const postNewFilmTag = () => {};

/* ---------------------- Logs ---------------------- */

/**
 * String for console logging error for bad connection
 * @param error
 * @return {string}
 */
const logConnectionError = (error) => {
  return `Not able to get connection ${error}`;
};

/* --------------------- Export --------------------- */

/**
 * Exporting all above methods
 * @type {{getAccess: function(), getListOfFilms: function(), getFilmById: function(), getFilmSource: function(), postNewFilm: function(), logConnectionError: function(*)}}
 */
module.exports = {
  getAccess,
  getFilteredListOfFilms,
  getFilmById,
  getFilmSource,
  postNewFilm,
  postNewFilmTag,
  logConnectionError
};

