#!/bin/bash

MOUNT_OR_UMOUNT=$1
TYPE_OF_DISK=$2

INIC_OUTPUT=1

if [ "$MOUNT_OR_UMOUNT" = "mount" ]; then 
  if [[ "$TYPE_OF_DISK" =~ ^(|all|films)$ ]]; then
    sudo mount /dev/sda1 /mnt/Cloud
    if [ $? -eq 0 ]; then  
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "Mounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Films\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK" =~ ^(|all|documents)$ ]]; then
    sudo mount /dev/sda2 /mnt/Documents
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "Mounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Documents\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK"  =~ ^(|all|others)$ ]]; then
    sudo cryptsetup luksOpen /dev/sda3 Others
    sudo mount /dev/mapper/Others /mnt/Others
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "Mounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Others\n"
    fi
  fi
elif [ "$MOUNT_OR_UMOUNT" = "umount" ]; then  
  if [[ "$TYPE_OF_DISK" =~ ^(|all|films)$ ]]; then
    sudo umount /mnt/Cloud
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "UnMounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Films\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK" =~ ^(|all|documents)$ ]]; then
    sudo umount /mnt/Documents
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "UnMounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Documents\n"
    fi
  fi
  if [[ "$TYPE_OF_DISK" =~ ^(|all|others)$ ]]; then
    sudo umount /mnt/Others
    if [ $? -eq 0 ]; then
      if [ $INIC_OUTPUT -ne 0 ]; then
        printf "UnMounted disk:\n"
        INIC_OUTPUT=0
      fi
      printf "* Others\n"
    fi
    sudo cryptsetup luksClose Others
  fi
else
  printf "warning:\nFirst parameter must be \"mount\" or \"umount\"\n"  
fi
