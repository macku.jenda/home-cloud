#!/bin/bash

FAN_LEVEL=$1
INIC=$2
CHECK_LEVEL='^(0|1)$'

if [ "$#" -eq  "2" ] ; then
  if [[ $INIC =~ "true" ]] ; then	
    sudo bash
    echo "17" > /sys/class/gpio/export 
    echo "out" > /sys/class/gpio/gpio17/direction
    exit
  else
    printf "error: Wrong value of second parameter\nnotice: Pass 'true' for inicialization\n"
  fi  
elif [ "$#" -gt "0" ] ; then
  if [[ $FAN_LEVEL =~ $CHECK_LEVEL ]] ; then
    echo "$FAN_LEVEL" > /sys/class/gpio/gpio17/value
  else
    printf "error: Wrong value of first parameter\nnotice: Pass '0' for off or '1'for on\n"
  fi
else
   printf "warning: Wrong number of parameters was passed\nnotice: First parameter On/Off - required; Second parameter Inic - optional\n"
fi
