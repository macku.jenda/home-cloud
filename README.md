Home Cloud
==========

Web application for managing my school files and others. This aplication distributes 
these files via private network and make files easy accesable from any of my devices.

Back-End
--------

The Back-End of the aplication is implemented with Node.js. As database is used PostgreSQL and 
initial scripts are in ``/back-end/sql``.

The application running on Raspbery Pi with rasbian.

Front-End
---------

The Front-End of the aplication is wrote in Angular TypeScript. All dev files are in ``/front-end/dev``
and build is in ``/front-end/dist``
