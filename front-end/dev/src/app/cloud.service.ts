import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FindFilmForm} from './shared/findFilmForm.model';

@Injectable({
  providedIn: 'root'
})
export class CloudService {
  private cloudUrl = 'http://192.168.3.124:3001/api/';

  constructor(private http: HttpClient) { }

  getListOfFilms() {
    return this.http.get(this.cloudUrl + 'film/list');
  }

  getFilmById(filmId: number) {
    return this.http.get(this.cloudUrl + `film/list/${filmId}`);
  }

  getFilmUrl(filmId: number): string {
    return this.cloudUrl + `film/stream/${filmId}`;
  }

  postFilteredFilms (findObj: FindFilmForm) {
    return this.http.post(this.cloudUrl + 'film/list/find', {
      name: findObj.name,
      year: findObj.year
      // TODO bookmarks and genres and so on!
    });
  }
}
