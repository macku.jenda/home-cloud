import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListOfVideosComponent } from './videos/find-film-form/list-of-videos/list-of-videos.component';
import { VideoComponent } from './videos/video/video.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FindFilmFormComponent } from './videos/find-film-form/find-film-form.component';
import { NewFilmFormComponent } from './videos/new-film-form/new-film-form.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'films/find', component: FindFilmFormComponent, children: [
    { path: 'list', component: ListOfVideosComponent }
  ] },
  { path: 'films/new', component: NewFilmFormComponent },
  { path: 'films/:id', component: VideoComponent },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
