import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Videogular2
import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';

// Components
import { AppComponent } from './app.component';
  import { HeaderComponent } from './header/header.component';
  import { DashboardItemComponent } from './dashboard/dashboard-item/dashboard-item.component';
  import { VideosComponent } from './videos/videos.component';
    import { FindFilmFormComponent } from './videos/find-film-form/find-film-form.component';
      import { ListOfVideosComponent } from './videos/find-film-form/list-of-videos/list-of-videos.component';
       import { ListOfVideosItemComponent } from './videos/find-film-form/list-of-videos/list-of-videos-item/list-of-videos-item.component';
    import { NewFilmFormComponent } from './videos/new-film-form/new-film-form.component';
    import { VideoComponent } from './videos/video/video.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListOfVideosComponent,
    VideosComponent,
    VideoComponent,
    ListOfVideosItemComponent,
    DashboardComponent,
    PageNotFoundComponent,
    DashboardItemComponent,
    FindFilmFormComponent,
    NewFilmFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
