import {EventEmitter, Injectable } from '@angular/core';
import {Observable} from 'rxjs/index';
import {HttpResponse} from '@angular/common/http';

import { Film } from './film.model';
import {CloudService} from '../cloud.service';
import {FindFilmForm} from '../shared/findFilmForm.model';

@Injectable({
  providedIn: 'root'
})
export class FilmService {
  filmSelected = new EventEmitter<Film>();
  filmsChanged = new EventEmitter<Film[]>();
  films: Film[] = [];

  constructor(private cloudService: CloudService) { }

  /**
   * Method to get access to copy of films array
   * @return {Film[]} - array of films
   */
  getFilms(findObj: FindFilmForm): Film[] {
    this.loadFilms(findObj, true);
    return this.films.slice();
  }

  /**
   * Get film details by id
   * @param {number} filmId
   */
  getFilm(filmId: number): Film {
    return this.films.filter((film: Film) => film.id === filmId)[0];
  }

  /**
   * d
   * @param {null} film
   */
  postNewFilm(film: null) {
    return;
  }

  /**
   * Method for load films from database
   * @param {FindFilmForm} findObj
   * @param {boolean} force - to force loading set to true
   */
  private loadFilms(findObj: FindFilmForm, force?: boolean): void {
    if (this.films.length === 0 || force ) {
      // this.cloudService.getListOfFilms()
      this.cloudService.postFilteredFilms(findObj)
        .subscribe(
          (response: Observable<HttpResponse<string>>) => {
            if (force) { this.films = []; }
            response.forEach((film: any) => {
              this.films.push(new Film(
                film.id,
                film.film_name,
                film.film_note,
                film.film_year,
                film.file_type_shortcut,
                film.film_next_film_id,
                film.film_prev_film_id,
                film.genre_name,
                film.film_tag_name)
              );
            });
            console.log(this.films);
            this.filmsChanged.emit(this.films.slice());
          },
          (error) => console.log(error)
        );
    }
  }
}
