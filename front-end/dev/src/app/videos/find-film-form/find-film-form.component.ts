import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-find-film-form',
  templateUrl: './find-film-form.component.html',
  styleUrls: ['./find-film-form.component.css']
})
export class FindFilmFormComponent implements OnInit {
  findFilmForm: FormGroup;

  constructor(private filmService: FilmService,
              private router: Router) { }

  ngOnInit() {
    this.findFilmForm = new FormGroup({
      'name': new FormControl(null),
      'year': new FormControl(null, [Validators.min(1920), Validators.max(2100)])
    });
  }

  onSubmit() {
    this.router.navigate(['/films', 'find', 'list'], { queryParams: this.findFilmForm.value});
  }
}
