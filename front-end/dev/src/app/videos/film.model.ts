/**
 * Model of film
 */
export class Film {
  public name: string;
  public description: string;
  public year: number;
  public fileType: string;
  public nextFilmId: number | null;
  public prevFilmId: number | null;
  public genres: Array<string> | null;
  public tags: Array<string> | null;

  /**
   * Constructor of film class
   * @param {number} id - id of the film
   * @param {string} film_name - name of the film
   * @param {string} film_note - description of the film
   * @param {string} film_year - year of film announcemen
   * @param {string} file_type_shortcut - type shortcut
   * @param {number|null} film_next_film_id - next film id
   * @param {number|null} film_prev_film_id - prev film id
   * @param {string|null} genre_name - film genres
   * @param {string|null} film_tag_name - film tags
   */
  constructor(public id: number,
              film_name: string,
              film_note: string,
              film_year: string,
              file_type_shortcut: string,
              film_next_film_id: number | null,
              film_prev_film_id: number | null,
              genre_name: string | null,
              film_tag_name: string | null) {
    this.name = film_name;
    this.description = film_note;
    this.year = +film_year.slice(0, 4);    // To get only year from TimeStamp and make it number
    this.fileType = file_type_shortcut;
    this.nextFilmId = film_next_film_id;
    this.prevFilmId = film_prev_film_id;
    this.genres = genre_name === null ? null : genre_name.split(', ').map(String);
    this.tags = film_tag_name === null ? null : film_tag_name.split(', ').map(String);
  }
}
